# Firefox Quantum Userchrome
This is my fork of [Hnaguski's userChrome.css](https://github.com/Hnaguski/userChrome.css) for Firefox Quantum (v58+)  
It has compatibility for using colors generated with [wal](https://github.com/dylanaraps/pywal) (or wpg if you change the import line)

![scrot1](https://ptpb.pw/oISt.png)

# Usage
Simply clone this repository then make a symbolic link to ~/.mozilla/firefox/yourfirefoxid.default/chrome/userChrome.css  
for example it would be something like the command below but with a *different* ID number for you  
`ln -s /home/kota/git/userchrome/userChrome.css /home/kota/.mozilla/firefox/z869u13q.default/chrome/userChrome.css`  

**HOW TO ENABLE THE SCROLLBAR** - To enable the scrollbar simply comment out line 437 through 443 (everything is quite well commented and you can enable and disable most features of this theme by commenting out selected parts)  

# License
Hnaguski licensed this under the MIT license and I will be keeping my changes under those terms.

# More screenshots

![scrot2](https://ptpb.pw/jY3j.png)
![scrot3](https://ptpb.pw/j7BT.png)
